<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/3 14:39
 */

ini_set('display_errors', 'on');
ini_set('display_startup_errors', 'on');
ini_set('memory_limit', '1G');

error_reporting(E_ALL);

! defined('BASE_PATH') && define('BASE_PATH', dirname(__DIR__, 1));

require_once BASE_PATH . '/vendor/autoload.php';
require_once BASE_PATH . '/app/common.php';

recordLog('server start', '_websocket_server_start');

$config = app\common\lib\Config::get('server');

// 启动服务
new \Vars3cf\Swoole($config);