<?php

// +----------------------------------------------------------------------
// | Swoole 配置
// +----------------------------------------------------------------------

return [
    'mode' => SWOOLE_PROCESS,
    'servers' => [
        // http服务配置
        [
            'name' => 'http', //服务名称
            'host' => '0.0.0.0', //监听地址
            'port' => 9501, //端口
            'sock_type' => SWOOLE_SOCK_TCP,
            'callbacks' => [
                'onRequest' => \app\listener\http\onRequest::class,
            ],
            'settings' => [
                'daemonize' => 0, //守护进程
                'worker_num' => 4, //设置启动的 Worker 进程数。
                'max_request' => 100000, // 最大请求数
                'log_file' => BASE_PATH . '/public/static/logs/' . date('Ymd') . '/http_server.txt', //日志
                'heartbeat_idle_time' => 600, // 表示一个连接如果600秒内未向服务器发送任何数据，此连接将被强制关闭
                'heartbeat_check_interval' => 60, // 表示每60秒遍历一次
                'socket_buffer_size' => 128 * 1024 *1024, //配置客户端连接的缓存区长度
                'buffer_output_size' => 10 * 1024 * 1024, //配置发送输出缓存区内存尺寸
            ],
        ],
        // websocket服务配置
        [
            'name' => 'ws', //服务名称
            'host' => '0.0.0.0', //监听地址
            'port' => 9502, //端口
            'sock_type' => SWOOLE_SOCK_TCP,
            'callbacks' => [
                'onOpen' => \app\listener\websocket\onOpen::class,
                'onMessage' => \app\listener\websocket\onMessage::class,
                'onClose' => \app\listener\websocket\onClose::class,
                'onRequest' => \app\listener\websocket\onRequest::class,
                'onTask' => \app\listener\websocket\onTask::class,
                'onFinish' => \app\listener\websocket\onFinish::class,
            ],
            'settings' => [
                'daemonize' => 0, //守护进程
                'worker_num' => 4, //设置启动的 Worker 进程数。
                'max_request' => 100000, //最大请求数
                'log_file' => BASE_PATH . '/public/static/logs/' . date('Ymd') . '/websocket_server.txt', //日志
                'heartbeat_idle_time' => 600, // 表示一个连接如果600秒内未向服务器发送任何数据，此连接将被强制关闭
                'heartbeat_check_interval' => 60, // 表示每60秒遍历一次
                'socket_buffer_size' => 128 * 1024 *1024, //配置客户端连接的缓存区长度
                'buffer_output_size' => 10 * 1024 * 1024, //配置发送输出缓存区内存尺寸
                'task_worker_num' => 4, //配置 Task 进程的数量
                'task_enable_coroutine' => true, //任务是否使用协程
                // 'enable_coroutine' => false, //自动协程
            ],
            'room' => [
                'table' => [
                    'room_rows'   => 4096,
                    'room_size'   => 2048,
                    'client_rows' => 8192,
                    'client_size' => 2048,
                ],
            ],
        ],
    ],
];