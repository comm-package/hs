
### 介绍
    hs 是基于 Swoole 构建的高性能协程框架

### 环境要求
    php-7.3 或更高版本
    gcc-4.8 或更高版本
    swoole4.8扩展 或更高版本
    
### 软件架构
    - app 应用
	    - api 模块
	        - Index 控制器
	        - Common 公共基础控制器
	    - common 公共文件
	        - lib 类库
	    - listener 监听服务
	    - Task 任务
    - config 配置文件
        - server 服务配置
    - public 公共
        - static 静态资源文件
            - logs 日志
	    - bin.php 服务启动项
    - hs.project.ini 进程配置文件
    
### 安装教程
    - git clone （克隆仓库）
    - cd /hs
    - php public/bin.php
    - websocket访问 ws://127.0.0.1:9502
    - http访问 127.0.0.1:9501
    
### 服务配置说明
    - cd app/hs/config/server.php 
    - vim daemonize 改为 1 则为后台守护进程，为了方便调试，默认0，
    - 控制台直接观察 （按Ctrl+C停止服务）
    
### 日志存放说明
    - public/static/logs
    - 访问方式：域名:端口/static/logs/20220606/_api_member_broadcast_data/21.txt
    - 日志注释说明
        _api_member_broadcast_data  开头_api,结尾_data 是接口请求的数据日志
        _api_member_broadcast_success  开头_api,结尾_success 是接口请求成功日志
        _api_member_broadcast_error  开头_api,结尾_error 是接口请求失败日志

### 房间事件
    - 加入房间
        $server->rooms->join($fd, 'room1');
    - 离开房间
        $server->rooms->leave($fd, 'room1');
    - 获取房间所有用户
        $rm = $server->rooms->getClients('room1');

    


