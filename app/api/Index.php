<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/3 15:29
 */

namespace app\api;


class Index extends Common
{
    public function index()
    {
        try {
            $fd = $this->request->fd;
            $getData = $this->request->get;
        } catch (\Throwable $e) {
            return $this->error($e->getMessage(), $e);
        }
        return $this->success('OK', ['fd' => $fd, 'get_data' => $getData]);
    }
}