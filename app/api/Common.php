<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/3 15:30
 */

namespace app\api;


use app\common\lib\Context;

class Common
{
    /**
     * http请求体
     * @var mixed|null
     */
    public $request;

    /**
     * http响应体
     * @var mixed|null
     */
    public $response;

    /**
     * 服务
     * @var mixed|null
     */
    public $server;

    public function __construct()
    {
        $this->request = Context::get('request');
        $this->response = Context::get('response');
        $this->server = Context::get('server');
    }

    /**
     * 成功返回请求结果
     * User: lmg
     * Date: 2022/5/27 15:31
     * @param string $message
     * @param array $data
     * @return mixed
     */
    public function success($message = 'OK', $data = [])
    {
        $data = ['code' => 0, 'message' => $message, 'data' => $data];
        // 记录日志 (日志存放目录 static\logs\20220527\_api_cache_write\07.txt)
        recordLog([
            'request_method' => $this->request->server['request_method'],
            'request_uri' => $this->request->server['request_uri'],
            'request_time' => date('Y-m-d H:i:s', $this->request->server['request_time']),
            'remote_addr' => $this->request->server['remote_addr'],
            'request_data' => $this->request->server['request_method'] == 'POST' ? $this->request->post : $this->request->get,
            'response_data' => $data,
        ], str_replace('/', '_', $this->request->server['request_uri']) . '_success');

        return $this->response->end(json_encode($data));
    }

    /**
     * 失败返回请求结果
     * User: lmg
     * Date: 2022/5/27 16:16
     * @param string $message
     * @param array|object $data
     * @return mixed
     */
    public function error($message = 'error', $data = [])
    {
        if (is_object($data)) {
            $getMessage = $data->getMessage() . ' ' . $data->getFile() . ' line ' . $data->getLine();
            $data = [];
            $system_error = true;
        }
        $data = ['code' => -1, 'message' => $message, 'data' => $data];
        // 记录日志 (日志存放目录 static\logs\20220527\_api_cache_write\07.txt)
        $errorInfo = [
            'host' => getenv('ENVIRONMENT') != 'production' ? 'test.http.j18.ws.in-monkeys.net' : 'http.j18.ws.in-monkeys.net',
            'request_method' => $this->request->server['request_method'],
            'request_uri' => $this->request->server['request_uri'],
            'request_time' => date('Y-m-d H:i:s', $this->request->server['request_time']),
            'remote_addr' => $this->request->server['remote_addr'],
            'request_data' => $this->request->server['request_method'] == 'POST' ? $this->request->post : $this->request->get,
            'response_data' => $data,
            'error_message' => isset($getMessage) ? $getMessage : '',
        ];
        recordLog($errorInfo, str_replace('/', '_', $this->request->server['request_uri']) . '_error');

        // 发送钉钉机器人
        /*if (isset($system_error)) {
            (new Dingding())->robotSend($errorInfo);
        }*/

        return $this->response->end(json_encode($data));
    }
}