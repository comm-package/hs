<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/5 11:08
 */

namespace app\listener\websocket;


class onTask
{
    public function __construct($server, $task)
    {
        echo "New AsyncTask[id={$task->worker_id}]" . PHP_EOL;
        //返回任务执行的结果
        $task->finish("{$task->data} -> OK");
    }
}