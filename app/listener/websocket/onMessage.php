<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/3 15:01
 */

namespace app\listener\websocket;


class onMessage
{
    public function __construct($server, $frame)
    {
        echo "receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}\n";
    }
}