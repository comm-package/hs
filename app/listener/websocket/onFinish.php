<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/5 11:08
 */

namespace app\listener\websocket;


class onFinish
{
    public function __construct($server, $task_id, $data)
    {
        echo "AsyncTask[{$task_id}] Finish: {$data}" . PHP_EOL;
    }
}