<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/3 15:02
 */

namespace app\listener\websocket;


class onOpen
{
    public function __construct($server, $request)
    {
        echo "server: handshake success with fd {$request->fd} \n";
    }
}