<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/3 15:02
 */

namespace app\listener\websocket;


class onRequest
{
    public function __construct($request, $response)
    {
        $response->end("request success! fd {$request->fd}");
    }
}