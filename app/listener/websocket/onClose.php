<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/3 15:00
 */

namespace app\listener\websocket;


class onClose
{
    public function __construct($server, $fd)
    {
        echo "client {$fd} closed\n";
    }
}