<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/3 14:58
 */

namespace app\listener\http;


use app\common\lib\Context;

class onRequest
{
    public function __construct($server, $request, $response)
    {
        // 使用 Chrome 浏览器访问服务器，会产生额外的一次请求，/favicon.ico，可以在代码中响应 404 错误。
        if ($request->server['path_info'] == '/favicon.ico' || $request->server['request_uri'] == '/favicon.ico') {
            $response->end();
            return;
        }
        // $response->end("http sever request success! fd {$request->fd}");

        // 设置跨域
        $response = $this->setCrossDomain($response);

        // 上下文
        Context::put('request', $request);
        Context::put('response', $response);
        Context::put('server', $server);

        // 判断路由是否正确
        $route = explode('/', trim($request->server['request_uri'], '/'));
        if (count($route) < 3) {
            $result = ['code' => -1, 'message' => 'Request does not exist'];
            $response->end(json_encode($result));
            return;
        }
        list($module, $controller, $action) = $route;

        //根据 $controller, $action 映射到不同的控制器类和方法
        /*if (!is_dir("app\index")) {
            $result = ['code' => -1, 'message' => 'Module does not exist'];
            $response->end(json_encode($result));
            return;
        }*/
        $controllerClass = "\\app\\" . $module . "\\" . ucfirst($controller);
        if (!class_exists($controllerClass)) {
            $result = ['code' => -1, 'message' => 'Controller does not exist'];
            $response->end(json_encode($result));
            return;
        }
        $controllerObject = new $controllerClass();
        if (!method_exists($controllerObject, $action)) {
            $result = ['code' => -1, 'message' => 'Method does not exist'];
            $response->end(json_encode($result));
            return;
        }
        $controllerObject->$action();
    }

    /**
     * 设置http跨域
     * @param $response
     * @return mixed
     */
    public function setCrossDomain($response)
    {
        $response->header('Content-Type', 'text/html; charset=utf-8');
        $response->header('Access-Control-Allow-Origin', '*');
        $response->header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
        $response->header('Access-Control-Allow-Credentials', 'true');
        $response->header('Access-Control-Allow-Headers', 'token,app-type,content-type,sign,X-Requested-With,X_Requested_With');
        return $response;
    }
}