<?php
// 这是系统自动生成的公共文件

/**
 * 记录日志
 * @param string|array $data 请求数据 ['param1' => data1...]
 * @param string $filename 文件目录
 * @param int $clear 是否清空日志 0
 * 访问系统错误日志 https://api.vars3cf.com/static/logs/20210604/system_exception/log.txt
 * 访问日志 https://api.vars3cf.com/static/logs/20210604/test/15.txt
 */
function recordlog($data = 'message', $filename = 'log', $file_time = 'hour', $clear = FILE_APPEND)
{
    $time = time();
    $dir = BASE_PATH . "/public/static/logs/" . date('Ymd') . '/' . $filename . "/"; #外层文件夹
    // 默认为小时份文件 ，否则保存为为天
    $hour = $file_time == 'hour' ? date('H', $time) . '.txt' : 'log.txt';
    if (!is_dir($dir)) {
        mkdir($dir, 0777, true);
    }

    $data = is_array($data) ? $data : (array)$data;
    $str = '';
    foreach ($data as $key => $val) {
        $str .= "{$key}: " . print_r($val, true) . PHP_EOL;
    }
    $init = "=======================================================";
    file_put_contents($dir . $hour, date("Y-m-d H:i:s", $time) . PHP_EOL . $init . PHP_EOL . $str . PHP_EOL . PHP_EOL, $clear);
}