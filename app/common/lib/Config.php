<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/8/3 16:18
 */

namespace app\common\lib;

/**
 * Class 获取配置文件
 * @package app\common\lib
 */
class Config
{
    /**
     * 读取配置文件
     * @param $name
     * @return mixed|string
     */
    public static function get($name)
    {
        // 切割键
        $keys = explode('.', $name);
        $file = $keys[0];
        if (!is_file(BASE_PATH . "/config/{$file}.php")) {
            return "配置文件不存在";
        }

        // 查找缓存中是否存在,有就读取缓存
        if (Context::get('configContext_' . $file)) {
            $config = Context::get('configContext_' . $file);
        } else {
            $config = require_once BASE_PATH . "/config/{$file}.php";
            Context::put('configContext_' . $file, $config);
        }

        if (count($keys) == 3) {
            return $config[$keys[1]][$keys[2]];
        } elseif (count($keys) == 2) {
            return $config[$keys[1]];
        }
        return $config;
    }
}